﻿using UnityEngine;
using System.Collections;

public class CreateStage : MonoBehaviour {


    public GameObject stage;
    public Transform stagepos;
    private Vector3 pos;
    private int nCntStageHeight = 0;
    private int nCntStageWidth = 0;

    public GameObject pawn;


	// Use this for initialization
	void Start () {

        for (nCntStageHeight = 0; nCntStageHeight < 8; nCntStageHeight++)
        {

            for (nCntStageWidth = 0; nCntStageWidth < 8; nCntStageWidth++)
            {

                // 盤面
                GameObject stageboard = GameObject.Instantiate(stage) as GameObject;

                // 盤面の位置設定
                stage.transform.position = stagepos.transform.position;

                pos.x = nCntStageWidth * 3;
                pos.y = 0;
                pos.z = nCntStageHeight * 3;

                stage.transform.position += pos;
            }
        }


        for (nCntStageWidth = 0; nCntStageWidth < 8; nCntStageWidth++)
        {

            // 盤面
            GameObject stageboard = GameObject.Instantiate(pawn) as GameObject;

            // ポーンの位置設定
            pawn.transform.position = stagepos.transform.position;
            

            pos.x = nCntStageWidth * 3;
            pos.y = 1;
            pos.z = 1 * 3;

            pawn.transform.position += pos;
        }


	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
