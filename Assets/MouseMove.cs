﻿using UnityEngine;
using System.Collections;

public class MouseMove : MonoBehaviour {


    private Vector3 StartPos;
    private Vector3 EndPos;
    private Vector3 move;
    private Vector3 mousepos;
    private Vector3 screenPos;
    private Vector3 movePos;
    public Vector3 screenPoint;
    public Vector3 offset;
    public Vector3 currentPosition;

    public Transform basepos;
    private bool bTach = false;
    private MoveControll pawn;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


         //タッチ判定
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit))
            {

                GameObject obj = hit.collider.gameObject;

                //座標設定
                screenPoint = Camera.main.WorldToScreenPoint(obj.transform.position);

                float x = Input.mousePosition.x;
                float y = Input.mousePosition.y;
                offset = obj.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(x, y, screenPoint.z));
                Vector3 currentScreenPoint = new Vector3(x, y, screenPoint.z);

                //ワールド座標に変換
                currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint) + offset;

                if (bTach == false)
                {
                    if (obj.gameObject.tag == "Pawn")
                    {

                        pawn = obj.GetComponent<MoveControll>();

                        //タッチ有効
                        pawn.SetTach(true);
                        //pawn.SetCheck(false);
                        bTach = true;

                    }
                }
                    
            }

            

        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            //タッチ無効
            pawn.SetTach(false);
            pawn.SetCheck(true);
            bTach = false;
        }
	}



}
